<?php

namespace App\Controller\Front;

use App\Entity\User;
use App\Form\Front\RegisterType;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="front_index_index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        return $this->render('front/index/index.html.twig');
    }

    /**
     * @Route("/register", name="front_index_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder,
                             TranslatorInterface $translator): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
            $this->getDoctrine()->getManager()->persist($user);
            try {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', $translator->trans('front.index.register.flash.success', []));
                return $this->redirectToRoute('front_index_index');
            } catch (Exception $exception) {
                $this->addFlash('danger', $translator->trans('front.index.register.flash.error', []));
            }
        }
        return $this->render('front/index/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="front_index_login")
     * @param Request $request
     * @return Response
     */
    public function login(Request $request): Response
    {
        return $this->render('front/index/login.html.twig');
    }
}